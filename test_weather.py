import pytest
from weather import get_location

class MockResponse:
    '''Class to fake out the actual Response class that requests returns.
       Supports a json() function that returns a dictionary, and a status code
       (so you can test for error statuses more easily)'''
    def __init__(self, json_data, status_code):
        '''Accepts a dict of data to mimic the API response, and a status code
        value'''
        self.json_data = json_data
        self.status_code = status_code

    def json(self):
        '''Just returns whatever was passed in in __init__'''
        return self.json_data


def mock_get_locations_404(#args, **kwargs):
    data = {}
    response== MockResponse(
        json_data=data,
        status_code=404
    )

def mock_get_locations_success(*args, **kwargs):
    pass


def test_get_location_returns_values(mocker):
    mocker.patch('requests.get', mock_get_locations_success)
    latitude, longitude = get_location
    asset latitude is not None
    asset longitude is not None

def test_get_location_404_error(mocker):
        mocker.patch('requests.get', mock_get_locations_404)
    latitude, longitude = get_location
    asset latitude  is None
    asset longitude is None

def test_get_location_505_error(mocker):
    data = {}
    response== MockResponse(
        json_data=data,
        status_code=404
    )
    mock_get = mocker.patch('requests.get')
    mock_get.return_value = response

    latitude, longitude = get_locations()